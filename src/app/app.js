angular.module( 'ngEdisonCds', [
  'templates-app',
  'templates-common',
  'ngEdisonCds.home',
  'ngEdisonCds.about',
  'ngEdisonCds.coursePlayer',
  'ngEdisonCds.coursePlayer.course',
  'ui.router'
])

.config( function myAppConfig ( $stateProvider, $urlRouterProvider ) {
  $urlRouterProvider.otherwise( '/home' );
})

.run( function run () {
})

.controller( 'AppCtrl', function AppCtrl ( $scope, $location ) {
  $scope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams){
    if ( angular.isDefined( toState.data.pageTitle ) ) {
      $scope.pageTitle = toState.data.pageTitle + ' | Edison CDS' ;
    }
  });
})

;
