angular.module( 'ngEdisonCds.coursePlayer.course', [
  'ui.router'
])

.config(function config( $stateProvider ) {
  $stateProvider.state( 'coursePlayer.coursePlayerCourse', {
    url: '/course',
    views: {
      "coursePlayerMain": {
        controller: 'CoursePlayerCourseController',
        templateUrl: 'courseplayer/course/course.tpl.html'
      }
    },
    data:{ pageTitle: 'Home | Course' }
  });
})

.controller( 'CoursePlayerCourseController', function CoursePlayerCourseController( $scope ) {

})

;
