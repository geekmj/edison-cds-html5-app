angular.module( 'ngEdisonCds.coursePlayer', [
  'ui.router',
  'ngEdisonCds.service'
])

.config(function config( $stateProvider ) {
  $stateProvider.state( 'coursePlayer', {
    abstract: true,
    url: '/cp',
    views: {
      "main": {
        controller: 'CoursePlayerController',
        templateUrl: 'courseplayer/courseplayer.tpl.html'
      }
    },
    data:{ pageTitle: 'Home' }
  });
})

.controller( 'CoursePlayerController', function CoursePlayerController( $scope, appService ) {
  appService.getUserInfo('test');
})

;
