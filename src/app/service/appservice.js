angular
    .module('ngEdisonCds.service',[])

    .factory('appService', [ '$log','$http', function($log, $http) {
        var cps = {};

        cps.getUserInfo = function (userId) {
          $log.debug("User Id " + userId + " | ");
          url="assets/getUserInfo.json";
          return $http.get(url, {cache: true});
        };

        cps.getCourseMap = function (courseId) {
          $log.debug("Course Id " + courseId + " | ");
          url="assets/getCourseMap.json";
          return $http.get(url, {cache: true});
        };

        cps.getLesson = function (lessonId) {
          $log.debug("Lesson Id " + lessonId + " | ");
          url="assets/getLesson.json";
          return $http.get(url, {cache: true});
        };

        cps.getAssessment = function(assessmentId) {
          $log.debug("Assessment Id " + assessmentId + " | ");
          url="assets/getAssessment.json";
          return $http.get(url, {cache: true});
        };

        cps.getTaskList = function(taskId) {
          $log.debug("Task Id " + taskId + " | ");
          url="assets/getTaskList.json";
          return $http.get(url, {cache: true});
        };

        return cps;

    }]);
