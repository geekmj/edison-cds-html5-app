#Dev Machine Setup Guide For Edison HTML5 CDS

-----

##Step 1

Clone / Checkout Source Code

Note: Temporary Git repo for framework used

For Git (Install Git command line from http://git-scm.com/)

On command line (terminal) run it:

    git clone https://username@bitbucket.org/geekmj/edison-cds-html5-app.git

Change username with your username

##Step 2

Install NodeJS, they have binary for almost all platform. [Click Here](http://www.nodejs.org)

##Step 3

On command line (terminal) run it:

For Mac / *nix

    sudo npm -g install grunt-cli karma bower

For Windows

    npm -g install grunt-cli karma bower

##Step 4

On Command line (terminal) run it:

    npm install

##Step 5

On Command line (terminal) run it:

    bower install

##Step 6

On Command line (terminal) run it:

     grunt watch



With All steps completed above, now you are good to go.. You can navigate to build folder and click on index.html to see it running.
